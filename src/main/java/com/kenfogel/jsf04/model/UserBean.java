package com.kenfogel.jsf04.model;

import java.io.Serializable;
import java.util.Objects;
import javax.enterprise.context.RequestScoped;
import javax.inject.Named;

/**
 * This is effectively the same bean as in examples 001, 002, 003 but now 
 * modified with just two annotations for use in a JSF application where CDI 
 * will be responsible for instantiating upon first use this eliminating the 
 * need to instantiate it with 'new'.
 * 
 * We now call it the backing bean for the form. These fields are bound to the
 * xhtml page by referencing the name, userBean, with the field name, such as
 * firstName, to become #{userBean.firstName}. It is in Request scope so that
 * every visitor gets their own bean.
 *
 * @author Ken Fogel
 */

@Named("userBean") // DO NOT USE @Managed, this annotation is obsolete now
@RequestScoped
public class UserBean implements Serializable {

    private String firstName;
    private String lastName;
    private String emailAddress;

    public UserBean() {
        firstName = "";
        lastName = "";
        emailAddress = "";
    }

    public UserBean(String first, String last, String emailAddress) {
        firstName = first;
        lastName = last;
        this.emailAddress = emailAddress;
    }

    public void setFirstName(String f) {
        firstName = f;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setLastName(String l) {
        lastName = l;
    }

    public String getLastName() {
        return lastName;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 17 * hash + Objects.hashCode(this.firstName);
        hash = 17 * hash + Objects.hashCode(this.lastName);
        hash = 17 * hash + Objects.hashCode(this.emailAddress);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final UserBean other = (UserBean) obj;
        if (!Objects.equals(this.firstName, other.firstName)) {
            return false;
        }
        if (!Objects.equals(this.lastName, other.lastName)) {
            return false;
        }
        return Objects.equals(this.emailAddress, other.emailAddress);
    }

    @Override
    public String toString() {
        return "User{" + "firstName=" + firstName + ", lastName=" + lastName + ", email=" + emailAddress + '}';
    }
}
