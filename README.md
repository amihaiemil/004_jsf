This application, one of four, requests the email, first name and last name of a user, stores it in a text file, and finally displays the input. All four versions are visually identical. These four samples go from Servlets thru JSP (Model 1 and Model 2) and end in a JSF. The web.xml contains the location for a text file that will be written to. The location is a folder named 'temp' in the root of whatever drive/volume you are working in. Adjust accordingly.

Before you can run this you must first clone https://gitlab.com/omniprof/web_project_dependencies.git. This contains a parent pom.xml that all my samples for the web rely on. Using Maven you want to use the Install goal. If you are using NetBeans you will find that selecting Run Maven has an Install option.

Edit
